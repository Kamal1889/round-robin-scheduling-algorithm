import java.text.DecimalFormat;  //to format output from average wait Time, and average turn around time
import java.util.Scanner;
import java.util.InputMismatchException;

public class tester {
	/*
	 * @authors Kamaljit Grewal and Manvir Uppal.
	 * 
	 * class Tester takes input values for number of processes, time quantum, burst times, and arrival times from the user.
	 * Uses that data to calculate time turn around and waiting times, and the averages of those both.
	 * Works in combination with class SinglyLinkedQueue's functions enqueue, dequeue, and first.
	 * 
	 */

	public static void main(String args[])  {
		/*
		 * Main function collects all user input, does calculations, and prints all output.
		 * Contains functions
		 * 
		 * @instanceVariable processCount holds the number of processes the CPU will perform.
		 * @InstanceVar quantumTime holds the max time, in milliseconds, that each process can receive at once while there are other processes waiting.
		 * @InstanceVar i and count are indices used in the for loops.
		 * @InstanceVar temp will hold a copy of quantumTime. If after a process' time slice, burst time remains, that leftover time is put in temp.
		 * 					For each process, whatever temp holds is added to sq (the completion time). It is then assigned to that process' turn around time.
		 * @InstanceVar sq holds completion time for a processes.
		 * 
		 * @InstanceVar burstTime[] holds the burst time for each process.
		 * @InstanceVar waitTime[] holds the wait time for each process.
		 * @InstanceVar arrivalTime[] holds the arrival time for each process.
		 * @InstanceVar turnAroundTime[] hold's each process' turn around time.
		 * @InstanceVar rem_burstTime[] if a process does not finish within the time quantum, the remaining burst time is stored here.
		 * 
		 * @InstanceVar averageTurnAroundTime will hold average turn around time.
		 * @InstanceVar averageWaitTime will hold average waiting time.
		 *
		 * @InstanceVar inputIsValid holds true or false, depending on whether a user inputs are valid.
		 * 
		 */
	
		SinglyLinkedQueue queue = new SinglyLinkedQueue(); //initialize an empty SinglyLinkedQueue object.
															//Will add processes to this queue in the form of Nodes.
		
		int processCount=0, i, quantumTime=0, count=0, temp=0, sq=0,burstTime[],waitTime[],arrivalTime[], turnAroundTime[],rem_burstTime[];  
		float averageWaitTime=0,averageTurnAroundTime=0;  
		
		boolean inputIsValid = true;

		Scanner s = new Scanner(System.in);  //scanner s scans user input
		
	do {  //prompt user for number of processes at least one time
			try {  //user might enter unexpected input so put the risky prompt in try {}
				System.out.print("Enter the number of processes = ");  
				processCount = s.nextInt(); 
				inputIsValid = true;  //if the previous 2 lines threw no exception, then so far the user's input must've been valid. Set true.

				if(processCount <= 0) {  //must be at least 1 process in the CPU, or else there is no output
					System.out.println("Please enter a positive number of processes.");
					inputIsValid = false;  //input was not valid so set false
				}
							
			} catch (InputMismatchException e) {  //catch a possible InputMismatchException, the most likely exception to be thrown
				System.out.println("Please enter input of data type int.");  //let the user know what's wrong
				inputIsValid = false;  //input was not valid so set false
			} catch (Exception e) {  //general Exception e to catch any other possible exception, which are unlikely...
										//but this keeps us safe from the program ending unexpectedly due to unexpected users
				System.out.println("Please enter valid input, for example: data of type int.");
				inputIsValid = false;
			}
			s.nextLine(); //start fresh with a new blank line on the scanner
		} while (inputIsValid == false);  //if the user entered mismatched input, an exception would be thrown and inputIsValid = false.
					//this do-while let's the user try at least once, then again until they enter a correct input type
		
		
		/*
		 * the number of elements in the arrays for burstTime, waitTime, arrivalTime, tat, and rem_burstTime is equal to number of processes, or processCount.
		 * initialize new integer arrays with length of processCount.
		 */
		burstTime = new int[processCount];  
		waitTime = new int[processCount];  
		arrivalTime = new int[processCount];
		tat = new int[processCount];  
		rem_burstTime = new int[processCount];  
		
		
		System.out.print("Enter the arrival time of each process\n");  //collect arrivalTime of each process from the user.
		for (i=0;i<processCount;i++) {  //collect as many arrivalTiems as there are processes
			do {
				try { //the do-while loop, and try-catch is much the same as the above code where we prompted user for processCount.
							//this is because answers to this prompt can throw the same kinds of exceptions as processCount.
					System.out.print("P"+i+" = ");  //print P for process and i for the process number so the user knows what to enter
					arrivalTime[i] = s.nextInt(); 
					inputIsValid = true;
					
					if(arrivalTime[i] < 0) {  //must arrive at 0 or later, or else infeasible
						System.out.println("Please enter a positive number > 0 for arrival time.");
						inputIsValid = false;  //input was not valid so set false
					}
				
				} catch (InputMismatchException e) {
					System.out.println("Please enter input of data type int.");
					inputIsValid = false;
				} catch (Exception e) {
					System.out.println("Please enter valid input, for example: data of type int.");
					inputIsValid = false;
				}
				s.nextLine();
			} while (inputIsValid == false);
		}

		
		
		/*
		 * collect burstTime of each process from the user.
		 * 	Functions much the same as above where we got arrivalTimes from user. Same for-loop, do-while, and try-catch.
		 * We rewrote this exception handling code 4 times (for processCount, arrivalTimes, BurstTimes, & timeQuantum) because,
		 * if a user enters incorrect input for burst time, we do not want to make them have to start again from the beginning
		 * with the first prompt for number of processes.
		 */
		System.out.print("Enter the burst time of the process\n");
			for (i=0;i<processCount;i++)  {  
				  
				do {
					try {
						System.out.print("P"+i+" = "); 
						burstTime[i] = s.nextInt(); 
						inputIsValid = true;
						
						if(burstTime[i] <= 0) {  //must take 1 second or longer, or else this logically doesn't make sense
							System.out.println("Please enter a positive number > 0 for burst time.");
							inputIsValid = false;  //input was not valid so set false
						}
						
					} catch (InputMismatchException e) {
						System.out.println("Please enter input of data type int.");
						inputIsValid = false;
					} catch (Exception e) {
						System.out.println("Please enter valid input, for example: data of type int.");
						inputIsValid = false;
					}
					s.nextLine();
				} while (inputIsValid == false);
				 
				queue.enqueue(burstTime[i]);  
		
				rem_burstTime[i] = burstTime[i];  
			} //end of i < process count loop 
			
		
		//collects TimeQuantum from user. Handles exceptions.
		do {
			try {
				System.out.print("Enter the time quantum: ");  
				quantumTime = s.nextInt(); 
				inputIsValid = true;
				
				if(quantumTime <= 0) {  //must take 1 second or longer, or else this logically doesn't make sense.
					System.out.println("Please enter a positive number > 0 for time quantum.");
					inputIsValid = false;  //input was not valid so set false
				}
				
			} catch (InputMismatchException e) {
				System.out.println("Please enter input of data type int.");
				inputIsValid = false;
			} catch (Exception e) {
				System.out.println("Please enter valid input, for example: data of type int.");
				inputIsValid = false;
			}
			s.nextLine();
		} while (inputIsValid == false);
		
		
		s.close(); //close scanner to prevent resource leak
 
		// Creating a while loop that will continue until there is no first element (Is an empty queue)
		while(queue.first() != -1)  
			{  	
				//Create a foor loop that will continue for the entire number of processes
				for (i=0,count=0;i<processCount;i++)  {  
					//assigning the quantum time to temp variiable 
					temp = quantumTime;  
					if(rem_burstTime[i] == 0)  // if the remainder of the burst time is 0 (The task has been completed) 
					{  
						count++;  // add 1 to the count variable 
						continue;  // exit loop
					}  
					/*
					 * If the left over burst time is greater then the quantum time (the process will continue after 
					 * this quantum and needs to be added to the back of the queue)
					 * */
					if(rem_burstTime[i]>quantumTime)  {
						//change the remainder of burst time to a quantum less then what it was
						rem_burstTime[i]= rem_burstTime[i] - quantumTime;  
						//move the process to the end of queue
						queue.enqueue(queue.dequeue() - quantumTime);
					}
					
					/*
					 * If the left over burst time is less then the quantum time (the process either needs to the full 
					 * quantum to be completed or may only use part of the quantum)
					 * */
					else  {
						
						// if the remainder of burst time is positive
						if(rem_burstTime[i]>=0)  
						{  
							temp = rem_burstTime[i]; //set temp to the left over time that is 0< and  <quantumTime
							rem_burstTime[i] = 0;  //remove the burst time remainder
							queue.dequeue();//remove from queue
						}  
					}
					sq += temp;  // add the temp value to the array of sq
					turnAroundTime[i] = sq;
				}  //end of for i<processCount
				
				if(processCount == count)  // if the loops have gone through all processes break
					break;  
			}  //end of while(queue.first() != -1) 
		
			//print out headers for different times
			 System.out.print("\nProcess\t\tArrival Time\t\tBurst Time\t\tTurnaround Time\t\tWaiting Time\n");
			 
			for(i=0;i<processCount;i++)  
			{  
				//using the appropriate formulas to calculate and take in to account turn around time, wait time, average wait time and average turn around time
				turnAroundTime[i] = turnAroundTime[i] - arrivalTime[i];  //my only change here!
				waitTime[i]=turnAroundTime[i]-burstTime[i];  
				if (waitTime[i] == 0) {
					waitTime[i] = 0;
				}
				averageWaitTime=averageWaitTime+waitTime[i];  
				averageTurnAroundTime=averageTurnAroundTime+turnAroundTime[i];  
				System.out.print((i+1)+"\t\t\t"+arrivalTime[i]+ "\t\t\t" + burstTime[i]+"\t\t\t"+turnAroundTime[i]+"\t\t\t"+waitTime[i]+"\n");  
			}  
			
			averageWaitTime = averageWaitTime/processCount;  
			averageTurnAroundTime = averageTurnAroundTime/processCount;  
			
			//formatting and printing average wait time and turn around time
			DecimalFormat avrgFormat = new DecimalFormat("#.00");
			System.out.println("\nAverage turnaround time = "+avrgFormat.format(averageTurnAroundTime));  
			System.out.println("\nAverage waiting time = "+ avrgFormat.format(averageWaitTime));
			
			
	}
	
}


####### HOW TO USE EACH ALGORITHM ####### 

# Round-Robin Scheduling Algorithm: 

Take files SinglyLinkedQueue.java and tester.java. Make sure they're in the same package, and put them somewhere like eclipse. Hit run on tester.java. You will be prompted in console for various input. Then, a nicely formatted table and more data below it will be printed in console.

# Shortest Job First Scheduling Algorithm


1. Run SJFpreemtive.java

2. System will prompt user for number of processes first 
  (integer value accepted only, if unrecognized response is given exception is thrown. User will be indicated to what type of value is accepted and will prompt again)

3. System will prompt user to enter the Arrival time, Burst Times for each individual process
  (integer value accepted only, if unrecognized response is given exception is thrown. User will be indicated to what type of value is accepted and will prompt again)

4. System will print what values were inputted and format into a chart.
   example: 

	User entered 3 processes with respected inputs for arrival and burst times 

	Process     	Arrival time     BurstTime
	Process 1    	0       	 8
	Process 2    	1       	 4
	Process 3    	2        	 1

5. System will calculate average wait time using SJF algorithm and will output a float value represented in milliseconds.

# Combined Round Robin and Priority Scheduling

Take files combined.java and tester.java. Make sure they're in the same package, and put them somewhere like eclipse. Hit run on tester.java. You will be prompted in console for various input. Then, a nicely formatted table and more data below it will be printed in console.



import java.util.LinkedList;


public class SinglyLinkedQueue {
	/*
	 * @authors Manvir Uppal and Kamaljit Grewal.
	 *
	 * class SinglyLinkedQueue implements a Linked List based Queue.
	 * @InstanceVars Node head stores Node at the front of the Linked-List based queue.
	 * @InstanceVars Node tail stores Node at the end of the Linked-List based queue.
	 * Includes class Node, and functions: enqueue, dequeue, size, first, and display.
	 *
	 */
	public Node head = null;
	public Node tail = null;
	
	private LinkedList queue = new LinkedList(); //initialize an empty Linked List.

	
	class Node {
		/*
		 * class nodes defines Node objects.
		 * @InstanceVar element is an integer. Holds the data inside a Node.
		 * @InstanceVar next is a Node object. It points to the Node that was added to the Linked List after this Node.
		 */
		int element;
		Node next;
		
		public Node(int element) {
			/*
			 * This constructor defines a Node object.
			 * @param int element, is the data the Node holds. It is inputted by the user.
			 */
			this.element = element;
			this.next = null;
		} //end of Node(int element) constructor
		
	} //end of class Node

	 
	public void enqueue(int element) {
		/*
		 * Function enqueue adds a Node to the end of the queue, the end of the Linked List. No return value.
		 * @param int element will be the data stored inside the Node to be added.
		 */
		Node newEndNode = new Node(element);  //make the data element into a Node
		
		if (head == null) {  //if the Linked-List/queue is empty, this new Node is the only Node, and is the head and tail
			head = newEndNode;  //make head and tail point to the new Node
			tail = newEndNode;
		}
		
		else { //if there are others Nodes already lined up in the Linked-List queue, then...
			tail.next = newEndNode;  //this maintains the FIFO order of the queue, make sure new Node is comes after the previous tail
			tail = newEndNode;   //and because this new Node is now the last Node, make tail point to it
		}
		
		queue.addLast(newEndNode);  //now, actually add the new Node to the end of the Linked List-based queue
	} //end of enqueue function

	
	public int dequeue() {
		/*
		 * Function dequeue removes the head Node, the Node at the front of the Linked List-based queue.
		 * @return the integer element that was stored in the head Node.
		 */
		if(head != null) {  //if the Linked List-based queue is not empty...
			int dequeuedHead = head.element;  //get the intger element stored inside the head Node
			head = head.next; //reassign head to the Node that's second in the queue, so that the original head can be taken by Garbage Collector
			return dequeuedHead; //returns first element in Linkedlist	
		}
		return -1;  //if there is no head Node, and head == null, return -1 to let the function caller know that the queue is empty.
	} //end of dequeue function
	
	public int first() {
		/*
		 * Function first returns the Node that is first in line to be serviced in the Linked List queue. Does not alter the queue.
		 * @return the integer data that the first Node in the queue holds.
		 */
		if (head == null) {  //if there are no Nodes in the Linked List queue, return -1 to let the function caller know
			return -1;
		}
		return head.element;  //gets the integer data inside the Node 
	} //end of first function

} //end of class SinglyLinkedQueue
